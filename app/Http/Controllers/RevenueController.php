<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Order;

class RevenueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request) {
        $no_of_months = 6;
        
        $from = $request->input('from');
        $to = $request->input('to');
        
        if($from && $to) {
            $from = Carbon::parse($from);
            $to = Carbon::parse($to);
        } else {
            $to = Carbon::now();
            $from = $to->copy()->addMonth($no_of_months);
        }
        
        
        $diffInMonths = $to->diffInMonths($from);
        $months = [];
        
        for($i=($diffInMonths-1); $i>=0; $i--) {
            /*$months[] = $now->copy()->subMonth($i)->month;*/
            $months[] = $to->copy()->month - $i;
        }
        
        $labels = [];
        $values = [];
        foreach($months as $month) {
            $values[] = Order::whereMonth('created_at', $month)
                                ->sum('total');
            $labels[] = date('F', mktime(0, 0, 0, $month, 10));;
        }
        
        /*--------country wise---------*/
        
        $countries = Order::select(['country'])
                            ->groupBy('country')
                            ->whereNotNull('country')
                            ->pluck('country')
                            ->toArray();
                            
        $country_values = [];                  
        foreach($countries as $country) {
            $country_values[$country] = Order::where('country', $country)
                                        ->sum('total');
        }
        arsort($country_values);
        
        $data = [
                    'labels' => $labels, 'values' => $values, 'from' => $from, 'to' => $to,
                    'countries_labels' => array_keys($country_values), 'country_values' => array_values($country_values)
                ];
        return view('dashboard.revenue')->with($data);
    }
}
