<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\ImageDocNotification;
use App\Order;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    public function addToNotification(array $parameters)
    {
        $description = $parameters['description'];
        $statusclass = $parameters['statusclass'];
        $doctor_id = $parameters['doctor_id'];
        $uid = $parameters['uid'];
        
        $notification = new ImageDocNotification;
        $notification->description = $description;
        $notification->statusclass = $statusclass;
        $notification->doctor_id = $doctor_id;
        $notification->uid = $uid; /*-------can be anything - orderid, doctorid, orderstatusid, etc----*/
        $notification->save();
        return;
    }
    
    public function getAllNotifications()
    {
        if (Auth::guard('imagedocuser')->check()) {
            $user = Auth::guard('imagedocuser')->user();
            $notification = ImageDocNotification::where('doctor_id', $user->id)
                                                ->orderBy('created_at', 'DESC')
                                                ->get();
            
            $orders_to_approve = Order::where('imagedoc_id',$user->id)
                                ->whereHas('order_status', function ($query) {
                                        $query->whereNull('approve');
                                    })
                                ->get();
            
            foreach($orders_to_approve as $order) {
                $approve_notification = new ImageDocNotification;
                $approve_notification->description = 'Your order #'.$order->id.' is awaiting your approval';
                $approve_notification->statusclass = 'warning';
                $approve_notification->doctor_id = $user->id;
                $approve_notification->viewed = null;
                
                $notification->push($approve_notification);
            }
            
            if(!$notification) {
                return null;
            }
            
            $count = 0;
            for($i=0; $i<$notification->count(); $i++) {
                if(!$notification[$i]->viewed) {
                    $count = $count + 1;
                }
            }
            
            /*$notification->count_unseen = $count;*/
                                        
            return [
                'notifications' => $notification,
                'count_unseen' => $count
                ];
        }
        
        return null;
    }
}
