<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\OrderStatus;
use DB;
use Auth;
use Log;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    public function show($id)
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $order = Order::where('imagedoc_id', $userid)
                        ->whereId($id)
                        ->first();
                        
        $services = json_decode($order->services,true);
        
        $serviceDetails = [];
        
        foreach($services as $key=>$service) {
            $serviceDetails[$key] = DB::table($key)
                                        ->where('order_id',$id)
                                        ->get();
        }

        $data = [
            'order' => $order,
            'services' => $services,
            'serviceDetails' => $serviceDetails
            ];
        
        return view('doc.orders.show')->with($data);
    }
    
    public function ShowOngoing() {
        $orders = $this->ProcessingOrdersDetails();
        $processing_orders = $this->ProcessingOrders();
        $awaiting_orders = $this->AwaitingApprovalOrders();
        $data = ['orders' => $orders, 'processing_orders' => $processing_orders, 'awaiting_orders' => $awaiting_orders];
        
        return view('doc.orders.ongoing')->with($data);
    }
    
    public function ShowHistory() {
        $orders = $this->CompletedOrdersDetails();
        $total_orders = $this->TotalOrders();
        $total_amount = $this->TotalAmount();
        $data = ['orders' => $orders, 'total_orders' => $total_orders, 'total_amount' => $total_amount];
        
        return view('doc.orders.previous')->with($data);
    }
    
    public function RecentOrders()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders = Order::where('imagedoc_id', $userid)
                        ->orderBy('id','desc')
                        ->take(5)
                        ->get();
        
        return $orders;
    }
    
    public function TotalOrders()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders_count = Order::where('imagedoc_id', $userid)
                        ->count();
        
        return $orders_count;
    }
    
    public function TotalAmount() {
        $userid = Auth::guard('imagedocuser')->user()->id;
                    
        $orders_total_amount = Order::where('imagedoc_id',$userid)
                                        ->sum('total');
        return $orders_total_amount;
    }
    
    public function ProcessingOrders()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders_count = Order::where('imagedoc_id', $userid)
                        ->whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'processing');
                        })
                        ->count();
        
        return $orders_count;
    }
    
    public function ProcessingOrdersDetails()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders = Order::where('imagedoc_id', $userid)
                        ->whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'processing');
                        })
                        ->get();
        
        return $orders;
    }
    
    public function CompletedOrders()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders_count = Order::where('imagedoc_id', $userid)
                        ->whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'completed');
                        })
                        ->count();
        
        return $orders_count;
    }
    
    public function CompletedOrdersDetails()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders = Order::where('imagedoc_id', $userid)
                        ->whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'completed');
                        })
                        ->get();
        
        return $orders;
    }
    
    public function AwaitingApprovalOrders()
    {
        $userid = Auth::guard('imagedocuser')->user()->id;
        
        $orders_count = Order::where('imagedoc_id', $userid)
                        ->whereHas('order_status', function ($query) {
                            $query->whereNull('approve');
                        })
                        ->count();
        
        return $orders_count;
    }
    
    public function Approve($id) {
        $order_status = OrderStatus::where('order_id',$id)
                                    ->first();
        $order_status->status = 'processing';
        $order_status->approve = 1;
        $order_status->save();
        return;
    }
}
