<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Order;
use DB;
use PDF;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    public function ShowInvoice($id) {
        $user = Auth::guard('imagedocuser')->user();
        
        $order = Order::where('imagedoc_id', $user->id)
                        ->whereId($id)
                        ->first();
        
        $services = json_decode($order->services,true);
        
        $serviceDetails = [];
        
        foreach($services as $key=>$service) {
            $serviceDetails[$key] = DB::table($key)
                                        ->where('order_id',$id)
                                        ->get();
        }
        
        $data = [
            'order' => $order,
            'user' => $user,
            'services' => $services,
            'serviceDetails' => $serviceDetails
            ];
        
        // return view('doc.orders.invoice')->with($data);
        
        $pdf = PDF::loadView('doc.orders.invoice', $data);
        
        return $pdf->setPaper('a4')->setOption('margin-bottom', 0)->setOption('margin-top', 2)->inline();
    }
}
