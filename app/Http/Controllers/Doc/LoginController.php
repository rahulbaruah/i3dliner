<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ImageDoc;
use Log;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLoginFormUsers(){
        return view('doc.login');
    }

    public function username(){
        return 'email';
    }

    function checklogin(Request $request){
        $this->validate($request, [
            'email' => 'required', 
            'password' => 'required',
        ]);
        
        $user = ImageDoc::where([
            'email' => $request->get('email'), 
            'password' => $request->get('password')
        ])->first();
        
        $remember = $request->get('remember');
        
        if($user)
        {
            Auth::guard('imagedocuser')->login($user, $remember);
            return redirect('doc/');
        } else {
            return redirect('doc/login');
        }
    }

    function logout(){
        Auth::guard('imagedocuser')->logout();
        return redirect('doc/login');
    }
}
