<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ImageDoc;
use Auth;
use Storage;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$user = Auth::guard('imagedocuser')->user();
        $user = ImageDoc::whereId($userid)
                ->first();*/
        $data = ['user' => Auth::guard('imagedocuser')->user()];
        return view('doc.profile.show')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'email' => 'required|email'
		]);
		
        $imagedoc = ImageDoc::find($id);
        
        if($imagedoc->email != $request->input('email')){
			$this->validate($request, [
				'email' => 'unique:imagedoc'
			]);
		}
        
        $imagedoc->name = $request->input('name');
        $imagedoc->email = $request->input('email');
        $imagedoc->mobile = $request->input('mobile');
        $imagedoc->address = $request->input('address');
        $imagedoc->save();
        
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            
            $path = $file->getRealPath();
            $originalName = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $extension = strtolower($file->getClientOriginalExtension()); // getting image extension
            $filesize = $file->getSize();
            $filesize = $filesize/1000000; /*-----MB------*/
            $fileName = 'profile_'.clean($originalName).'_'.time().'.'.$extension; // renaming image
            
            /*---------check filetype----------*/
            if (!($extension==('jpg'||'jpeg'||'png'||'gif'))) {
                $msg = ['Please provide image in the following formats (jpg, png, gif)'];
                return redirect()->back()->with(['msg' => $msg]);
            }
            /*--------check filesize------------*/
            if ($filesize>3) {    /*---------3MB-------*/
                $msg = ['Please provide an image of size less than 3MB'];
                return redirect()->back()->with(['msg' => $msg]);
            }
            
            $profile_image_path = 'users/'.$id.'/profile/';
            
            Storage::disk('public')->put($profile_image_path.$fileName, file_get_contents($path));
            
            $imagedoc->image = $profile_image_path.$fileName;
            $imagedoc->save();
        }
        
        $data = ["msg" => ["Profile updated successfully"]];
		return redirect()->back()->with($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
