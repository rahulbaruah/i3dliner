<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    public function index() {
        $data = ['user' => Auth::guard('imagedocuser')->user()];
        return view('doc.account.summary')->with($data);
    }
}
