<?php

namespace App\Http\Controllers\Doc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Doc\OrdersController;
use App\Http\Controllers\Doc\NotificationController;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('imagedocauth');
    }
    
    public function index()
    {
        $orders = (new OrdersController)->RecentOrders();
        
        $total_orders = (new OrdersController)->TotalOrders();
        $completed_orders = (new OrdersController)->CompletedOrders();
        $processing_orders = (new OrdersController)->ProcessingOrders();
        $awaiting_orders = (new OrdersController)->AwaitingApprovalOrders();
        
        $notifications = (new NotificationController)->getAllNotifications();

        $data = ['orders' => $orders, 'total_orders' => $total_orders, 'completed_orders' => $completed_orders, 'processing_orders' => $processing_orders, 'awaiting_orders' => $awaiting_orders, 'notifications' => $notifications['notifications']];
        return view('doc.index')->with($data);
    }
}
