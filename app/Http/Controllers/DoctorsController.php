<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageDoc;
use App\Order;

class DoctorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
        $doctors = ImageDoc::orderBy('id','desc')
                            ->get();
                            
        /*-----Top Doctors---------*/
        $imagedoc_ids = Order::all()
                            ->groupBy('imagedoc_id')
                            ->toArray();
                            
        $imagedocs = [];
        
        foreach($imagedoc_ids as $key=>$value) {
            $doc_name = ImageDoc::whereId($key)
                                    ->first()->name;
            $imagedocs[$doc_name] = Order::where('imagedoc_id',$key)
                                        ->sum('total');
        }
        arsort($imagedocs);
        $topDocs = array_slice($imagedocs, 0, 5, true);
        
        /*-------Top Countries-------*/
        
        $countries = Order::select(['country'])
                            ->groupBy('country')
                            ->whereNotNull('country')
                            ->pluck('country')
                            ->toArray();
                            
        $country_values = [];                  
        foreach($countries as $country) {
            $country_values[$country] = Order::where('country', $country)
                                        ->sum('total');
        }
        arsort($country_values);
        $topCountries = array_slice($country_values, 0, 5, true);
        
        $data = ['doctors' => $doctors, 'topDocs' => $topDocs, 'topCountries' => $topCountries];
        return view('dashboard.doctors.index')->with($data);
    }
    
    public function show($id) {
        $doctor = ImageDoc::find($id);
        $data = ['doctor' => $doctor];
        return view('dashboard.doctors.show')->with($data);
    }
    
    public function fix() {
        $imagedoc = ImageDoc::all();
        foreach($imagedoc as $doc) {
            $order = Order::where('email',$doc->email)
                            ->update(['imagedoc_id' => $doc->id]);
        }
        
        return "Success";
    }
}
