<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderStatus;
use DB;
use Log;
use App\Http\Controllers\Doc\NotificationController;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index() {
        $processing_order_details = $this->ProcessingOrderDetails();
        $total_orders = (new OrdersController)->TotalOrders();
        $completed_orders = (new OrdersController)->CompletedOrders();
        $processing_orders = (new OrdersController)->ProcessingOrders();
        $cancelled_orders = (new OrdersController)->CancelledOrders();
        $cancelled_order_details = (new OrdersController)->CancelledOrderDetails();
        
        $data = ['processing_order_details' => $processing_order_details, 'total_orders' => $total_orders, 'completed_orders' => $completed_orders, 'processing_orders' => $processing_orders, 'cancelled_orders' => $cancelled_orders, 'cancelled_order_details' => $cancelled_order_details];
        return view('dashboard.orders.index')->with($data);
    }
    
    public function show($id) {
        $order = Order::find($id);
        $services = json_decode($order->services,true);
        
        $serviceDetails = [];
        
        foreach($services as $key=>$service) {
            $serviceDetails[$key] = DB::table($key)
                                        ->where('order_id',$id)
                                        ->get();
        }

        $data = [
            'order' => $order,
            'services' => $services,
            'serviceDetails' => $serviceDetails
            ];
        return view('dashboard.orders.show')->with($data);
    }
    
    public function ShowAllOrders(Request $request) {
        $status = $request->input('status');
        
        $from = $request->input('from');
        $to = $request->input('to');
        
        if($status == 'cancelled') {
            $query = Order::whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'cancelled');
                        })
                        ->orderBy('id','desc');
        } else {
            $query = Order::orderBy('id','desc');
        }
        
        if($from && $to) {
            $query->whereBetween('created_at', [$from, $to]);
        }
        
        $orders = $query->get();
        
        $data = [
            'orders' => $orders,
            'status' => $status,
            'from' => $from,
            'to' => $to
            ];
        return view('dashboard.orders.allorders')->with($data);
    }
    
    public function NewOrders() {
        $orders = Order::whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'pending');
                        })
                        ->orderBy('id','desc')
                        ->take(3)
                        ->get();
        
        return $orders;
    }
    
    public function TotalOrders() {
        $orders_count = Order::count();
        
        return $orders_count;
    }
    
    public function CompletedOrders() {
        $orders_count = OrderStatus::where('status', 'completed')
                                ->count();
        
        return $orders_count;
    }
    
    public function CompletedOrderDetails() {
        $orders = Order::whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'completed');
                        })
                        ->orderBy('id','desc')
                        ->get();
        
        return $orders;
    }
    
    public function ProcessingOrders() {
        $orders_count = OrderStatus::where('status', 'processing')
                                ->count();
        
        return $orders_count;
    }
    
    public function ProcessingOrderDetails() {
        $orders = Order::whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'processing');
                        })
                        ->orderBy('id','desc')
                        ->get();
        
        return $orders;
    }
    
    public function CancelledOrders() {
        $orders_count = OrderStatus::where('status', 'cancelled')
                                ->count();
        
        return $orders_count;
    }
    
    public function CancelledOrderDetails() {
        $orders = Order::whereHas('order_status', function ($query) {
                            $query->where('status', '=', 'cancelled');
                        })
                        ->orderBy('id','desc')
                        ->get();
        
        return $orders;
    }
    
    public function StatusUpdate(Request $request, $id) {
        $status = $request->input('status');
        
        $order_status = OrderStatus::where('order_id',$id)
                                    ->first();
        $order_status->status = $status;
        $order_status->save();
        return;
    }
    
    public function PhaseUpdate($id) {
        $order_status = OrderStatus::where('order_id',$id)
                                    ->first();
        $order_status->phase = (int)$order_status->phase + 1;
        $order_status->save();
        return;
    }
    
    public function FixOrderStatus() {
        $orders = Order::all();
        foreach($orders as $order) {
            OrderStatus::create([
                'order_id' => $order->id,
                'status' => 'pending'
                ]);
        }
        return 'Success';
    }
    
    public function ShowUploadLink($id) {
        $order = Order::find($id);
        $data = ['order' => $order];
        return view('dashboard.orders.upload')->with($data);
    }
    
    public function UploadLink(Request $request, $id) {
        $order = Order::find($id);
        
        $order_status = OrderStatus::where('order_id',$id)
                                ->first();
        $order_status->case_link = $request->input('case_link');
        $order_status->save();
        
        $data = ['msg' => ['Case link added successfully']];
        return redirect()->back()->with($data);
    }
    
    public function InvalidFileUpdate($id) {
        $order = Order::find($id);
        
        (new NotificationController)->addToNotification([
            'description' => 'Invalid file for Order id #'.$id.', Kindly re-upload the files',
            'statusclass' => 'danger',
            'doctor_id' => $order->imagedoc_id,
            'uid' => $id
            ]);
        
        return;
    }
}
