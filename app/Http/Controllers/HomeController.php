<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\OrdersController;
use App\Order;
use App\ImageDoc;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders = (new OrdersController)->NewOrders();
        $total_orders = (new OrdersController)->TotalOrders();
        $completed_orders = (new OrdersController)->CompletedOrders();
        $processing_orders = (new OrdersController)->ProcessingOrders();
        
        /*-----Top Doctors---------*/
        $imagedoc_ids = Order::all()
                            ->groupBy('imagedoc_id')
                            ->toArray();
                            
        $imagedocs = [];
        
        foreach($imagedoc_ids as $key=>$value) {
            $doc_name = ImageDoc::whereId($key)
                                    ->first()->name;
            $imagedocs[$doc_name] = Order::where('imagedoc_id',$key)
                                        ->sum('total');
        }
        arsort($imagedocs);
        $topDocs = array_slice($imagedocs, 0, 5, true);
        
        /*-------Top Countries-------*/
        
        $countries = Order::select(['country'])
                            ->groupBy('country')
                            ->whereNotNull('country')
                            ->pluck('country')
                            ->toArray();
                            
        $country_values = [];                  
        foreach($countries as $country) {
            $country_values[$country] = Order::where('country', $country)
                                        ->sum('total');
        }
        arsort($country_values);
        $topCountries = array_slice($country_values, 0, 5, true);
        
        $services = [];
        $service_count = [];
        /*---Best Selling Services---*/
        $services['bone_segmentation'] = DB::table('bone_segmentation')
                                            ->sum('subtotal');
        $service_count['bone_segmentation'] = DB::table('bone_segmentation')
                                                ->count();
        
        $services['treatment_planning_design'] = DB::table('treatment_planning_design')
                                                    ->sum('subtotal');
        $service_count['treatment_planning_design'] = DB::table('treatment_planning_design')
                                                        ->count();
                                                
        $services['treatment_planning_design_fabrication'] = DB::table('treatment_planning_design_fabrication')
                                                                ->sum('subtotal');
        $service_count['treatment_planning_design_fabrication'] = DB::table('treatment_planning_design_fabrication')
                                                                    ->count();
                                                
        $services['digital_denture'] = DB::table('digital_denture')
                                            ->sum('subtotal');
        $service_count['digital_denture'] = DB::table('digital_denture')
                                                ->count();                                    
                                            
        $services['orthodentic_treatment'] = DB::table('orthodentic_treatment')
                                                ->sum('subtotal');
        $service_count['orthodentic_treatment'] = DB::table('orthodentic_treatment')
                                                        ->count();                                        
                                                
        $services['radiology_report'] = DB::table('radiology_report')
                                                ->sum('subtotal');
        $service_count['radiology_report'] = DB::table('radiology_report')
                                                ->count();                                        
                                                
        $services['metal_components'] = DB::table('metal_components')
                                                ->sum('subtotal');
        $service_count['metal_components'] = DB::table('metal_components')
                                                ->count();                                        
                                                
        $services['zygoma_guided_surgery'] = DB::table('metal_components')
                                                ->sum('subtotal');
        $service_count['zygoma_guided_surgery'] = DB::table('zygoma_guided_surgery')
                                                    ->count();                                        
                                                
        arsort($services);
        
        $data = ['orders' => $orders, 'total_orders' => $total_orders, 'completed_orders' => $completed_orders, 'processing_orders' => $processing_orders, 'topDocs' => $topDocs, 'topCountries' => $topCountries, 'services' => $services, 'service_count' => $service_count];
        return view('dashboard.index')->with($data);
    }
}
