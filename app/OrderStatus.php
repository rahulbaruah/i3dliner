<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = 'order_status';
    
    protected $fillable = ['order_id','status','phase'];
    
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
