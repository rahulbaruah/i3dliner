<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageDocNotification extends Model
{
    protected $table = 'imagedoc_notifications';
}
