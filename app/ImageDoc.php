<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class ImageDoc extends Authenticatable
{
    use Notifiable;
    
    // public $timestamps = false; 
    protected $table = 'imagedoc';
    
    protected $fillable = [
        'email', 'password',
    ];
    
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function orders()
    {
        return $this->hasMany('App\Order', 'email', 'email');
    }
    
    public function getTotalOrdersAttribute()
    {
        return $this->orders->count();
    }
    
    public function getTotalAmountAttribute()
    {
        return Order::where('imagedoc_id',$this->id)
                        ->sum('total');
    }
}
