<?php

function showServicesFromArray($services) {
    $services = json_decode($services,true);
    $temp_service_key = [];
    foreach($services as $key=>$service) {
        $temp_service_key[] = ucwords(str_replace("_", " ", $key));
    }
    return implode(', ', $temp_service_key);
}

function styleString($string) {
    return ucwords(str_replace("_"," ",$string));
}

function firstName($string) {
    $arr = explode(' ', $string);
    $num = count($arr);
    
    $first_name = '';
    //$middle_name = '';
    $last_name = '';
    
    if ($num == 1) {
        return $string;
    }
    
    $last_name = $arr[$num-1];
    
    for($i=0; $i<($num-1); $i++) {
        $first_name = $first_name.$arr[$i].' ';
    }
    
    return trim($first_name);
}

function array_flatten($array) { 
  if (!is_array($array)) { 
    return FALSE; 
  } 
  $result = array(); 
  foreach ($array as $key => $value) { 
    if (is_array($value)) { 
      $result = array_merge($result, array_flatten($value)); 
    } 
    else { 
      $result[$key] = $value; 
    } 
  } 
  return $result; 
}

function invoice($user_id, $order_id){
	$date = date('Ymd', time());
	return $date.'-'.$user_id.$order_id;
}

function now_date() {
  return (date("d-M-Y"));
}

function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}