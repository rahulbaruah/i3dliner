<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    
    protected $dates = [
        'created_at',
        'updated_at',
        'surgery_date'
    ];
    
    public function order_status()
    {
        return $this->hasOne('App\OrderStatus');
    }
    
}
