@extends('doclayouts.master')

@section('page-title', 'Profile')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/doc">Home</a></li>
    <li class="breadcrumb-item"><a href="/doc/account">Account Summary</a></li>
    <li class="breadcrumb-item active">Profile</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form class="form-horizontal" enctype="multipart/form-data" action="/doc/profile/{{$user->id}}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="MAX_FILE_SIZE" value="5000000">
        <div class="row">
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{$user->image ?  asset('storage/'.$user->image) : '/images/NoImage.png'}}"
                       alt="User profile picture">
                </div>

                <!--<h3 class="profile-username text-center">Nina Mcintire</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Followers</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li>
                </ul>-->

                <!--<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
                <div class="form-group">
                    <input name="image" type="file" class="form-control-file" id="profileImage">
                  </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-8">
            <div class="card">
              
              <div class="card-body">
                
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input name="name" type="text" class="form-control" id="inputName" placeholder="Name" value="{{$user->name}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email" value="{{$user->email}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputMobile" class="col-sm-2 col-form-label">Mobile</label>
                        <div class="col-sm-10">
                          <input name="mobile" type="text" class="form-control" id="inputMobile" placeholder="Mobile no" value="{{$user->mobile}}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputAddress" class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                          <textarea name="address" class="form-control" id="inputAddress" placeholder="Address">{!!$user->address!!}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>
                    
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        </form>
        <!-- /.row -->
        
    </div>
  </section>
@endsection