@extends('doclayouts.master')

@section('page-title', 'Doctor Dashboard')

@section('style')
  @parent
  <!--<link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">-->
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
  <style type="text/css">
    .products-list .product-info {
      margin-left: 10px;
  }
  </style>
@endsection

@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active">Starter Page</li>
</ol>-->
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @foreach($notifications as $notification)
        <div class="alert alert-{{$notification->statusclass ? $notification->statusclass : 'primary'}} alert-dismissible fade show" role="alert">
          {{$notification->description}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endforeach
        
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <p>Total Orders</p>
                <h3>{{ $total_orders }}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <p>Ongoing Orders</p>
                <h3>{{ $processing_orders }}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <p>Completed Orders</p>
                <h3>{{ $completed_orders }}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <p>Awaiting Approval</p>
                <h3>{{ $awaiting_orders }}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        
        <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Recent Orders</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table id="recent_orders_table" class="table m-0">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Service</th>
                      <th>Order Date</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($orders as $order)
                      <tr>
                        <td>{{$order->id}}</td>
                        <td>{{showServicesFromArray($order->services)}}</td>
                        <td>{{$order->created_at->format('d/m/Y')}}</td>
                        <td><span class="badge badge-default">{{$order->status ? $order->status : 'pending'}}</span></td>
                        <td>
                          <div class="btn-group" role="group">
                            <a href="/doc/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                            @if($order->order_status->case_link)
                              <a href="{{$order->order_status->case_link}}" target="_blank" class="btn btn-success btn-sm">Case Links for Approval</a>
                            @else
                              <button type="button" class="btn btn-success btn-sm" disabled>Case Links for Approval</button>
                            @endif
                            @if(!$order->order_status->approve)
                            <button data-id="{{$order->id}}" type="button" class="btn btn-danger btn-sm orderApproveBtn">Approve</button>
                            @else
                            <button type="button" class="btn btn-danger btn-sm" disabled title="Already Approved">Approved</button>
                            @endif
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
              </div>-->
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        
    </div>
  </section>
@endsection

@section('script')
  @parent
<!--<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>-->
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript">
  /*$("#recent_orders_table").DataTable({
    	"aaSorting": [],
        "columnDefs": [
            {
            "targets": 4,
            "orderable": false
            }
            ]
    });*/
    
    $(document).on('click','.orderApproveBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Approve this order',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/doc/order/'+$order_id+'/approve',
                		        success:function(data){
                		            window.location.reload();
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              /*$.alert('Canceled!');*/
              return;
            }
        }
      });
      return;
    });
</script>
@endsection