@extends('doclayouts.master')

@section('page-title', 'Profile')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/doc">Home</a></li>
    <li class="breadcrumb-item active">Account</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{$user->image ?  asset('storage/'.$user->image) : '/images/NoImage.png'}}"
                       alt="User profile picture">
                </div>

                <!--<h3 class="profile-username text-center">User Name</h3>-->

                <!--<p class="text-muted text-center">Software Engineer</p>-->

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Name</b> <a class="float-right">{{$user->name}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Contact</b> <a class="float-right">{{$user->mobile}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$user->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Shipping Address</b> <a class="float-right">{{$user->address}}</a>
                  </li>
                </ul>

                <a href="/doc/profile" class="btn btn-primary btn-block"><b>Edit <i class="fas fa-pencil-alt"></i></b></a>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-8">
            <div class="card">
              
              <div class="card-body">
                
                <div class="row">
                <div class="col-lg-4">
              <div class="small-box bg-info">
                <div class="inner">
                  <p>Total Orders</p>
                  <h3>{{$user->total_orders}}</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="small-box bg-success">
                <div class="inner">
                  <p>Total Purchase Amount</p>
                  <h3>$ {{$user->total_amount}}</h3>
                </div>
              </div>
            </div>
            </div>
            
            <ul>
            <li>
            <a href="/doc/new_order">
              <p>
                New Order
              </p>
            </a>
          </li>
          <li>
            <a href="/doc/orders">
              <p>
                Ongoing Orders
              </p>
            </a>
          </li>
          <li >
            <a href="/doc/history">
              <p>
                Order History
              </p>
            </a>
          </li>
            </ul>
            <a href="#" class="btn btn-info btn-block"><b>Billing Info</b></a>
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        
    </div>
  </section>
@endsection