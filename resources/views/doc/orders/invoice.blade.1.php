<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>i3d invoice</title>
    
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr.information td:nth-child(2), .invoice-box table tr.headerinfo td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    /*.invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }*/
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr class="headerinfo">
                            <td class="title">
                                <img src="/images/i3dlogo.png" style="width:100%; max-width:200px;">
                            </td>
                            
                            <td>
                                <h1>Invoice</h1>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Pub-Sarania Road, Bylane-3<br>
                                H.N:81, 1st Floor, Niva Villa<br>
                                Guwahati, Assam - 781003<br/>
                                India
                            </td>
                            
                            <td>
                                Order: #{{$order->id}}<br>
                                Invoice: #{{invoice($user->id,$order->id)}}<br>
                                Created: {{now_date()}}<br>
                                Due: {{now_date()}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="heading">
                            <td>
                                Date
                            </td>
                            <td>
                                Description
                            </td>
                            <td>
                                Quantity
                            </td>
                            <td>
                                Price
                            </td>
                            <td>
                                Amount
                            </td>
                        </tr>
                        <tr class="item">
                            <td>
                                {{$order->created_at->format('d/m/Y')}}
                            </td>
                            <td>
                                {{showServicesFromArray($order->services)}}
                            </td>
                            <td>
                                1
                            </td>
                            <td>
                                $ {{$order->total}}
                            </td>
                            <td>
                                $ {{$order->total}}
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <strong>Sub-Total:</strong>
                            </td>
                            <td>
                               <strong>$ {{$order->total}}</strong>
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <strong>Shipping:</strong>
                            </td>
                            <td>
                               <strong>$ 0</strong>
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <strong>Amount Paid:</strong>
                            </td>
                            <td>
                               <strong>$ 0</strong>
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <strong>Amount Due:</strong>
                            </td>
                            <td>
                               <strong>$ {{$order->total}}</strong>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>