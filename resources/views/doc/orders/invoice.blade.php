<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>i3d invoice</title>
    
    <style>
    body {
        margin:0;
    }
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr.information td:nth-child(2), .invoice-box table tr.headerinfo td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    /*.invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }*/
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    
    .service_details h4,.service_details p{
        margin:0;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr class="headerinfo">
                            <td class="title">
                                <img src="{{asset('/images/i3dlogo.png')}}" style="width:100%; max-width:200px;">
                            </td>
                            
                            <td>
                                <h1>Invoice</h1>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Pub-Sarania Road, Bylane-3<br>
                                H.N:81, 1st Floor, Niva Villa<br>
                                Guwahati, Assam - 781003<br/>
                                India
                            </td>
                            
                            <td>
                                Order: #{{$order->id}}<br>
                                Invoice: #{{invoice($user->id,$order->id)}}<br>
                                Created: {{now_date()}}<br>
                                Due: {{now_date()}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <table cellpadding="0" cellspacing="0">
                        <tr class="heading">
                            <td>
                                Date
                            </td>
                            <td colspan="4">
                                Description
                            </td>
                            <!--<td>
                                Quantity
                            </td>
                            <td>
                                Price
                            </td>
                            <td>
                                Amount
                            </td>-->
                        </tr>
                        <tr class="item">
                            <td>
                                {{$order->created_at->format('d/m/Y')}}
                            </td>
                            <td colspan="4">
                                <div class="service_details">
            @foreach($services as $key=>$service)
            
            @if($key == 'bone_segmentation')
                <h4><strong><u>Bone Segmentation</u></strong></h4>
                @foreach($serviceDetails[$key] as $bone_segmentation)
                <?php $bone_segmentation_cost = json_decode($bone_segmentation->cost,true); ?>
                
                    @if($bone_segmentation->basic_conversion == 1)
                        <p>Basic Conversion : $ {{$bone_segmentation_cost['basic_conversion']}}</p>
                    @endif
                    @if($bone_segmentation->virtual_extraction == 1)
                        <p>Virtual Extraction : $ {{$bone_segmentation_cost['virtual_extraction']}}</p>
                    @endif
                    @if($bone_segmentation->zygomatic_conversion == 1)
                        <p>Zygomatic Conversion : $ {{$bone_segmentation_cost['zygomatic_conversion']}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'treatment_planning_design')
                <h4><strong><u>Treatment Planning (Design Only)</u></strong></h4>
                @foreach($serviceDetails[$key] as $treatment_planning_design)
                <?php $treatment_planning_design_cost = json_decode($treatment_planning_design->cost,true); ?>
                
                    @if($treatment_planning_design->bone_reduction_case == 1)
                        <p>Bone Reduction Case : $ {{$treatment_planning_design_cost['bone_reduction_case']}}</p>
                    @endif
                    @if($treatment_planning_design->tooth_supported_case == 1)
                        <p>Tooth Supported Case : $ {{$treatment_planning_design_cost['tooth_supported_case']}}</p>
                    @endif
                    @if($treatment_planning_design->tissue_supported_case == 1)
                        <p>Tissue Supported Case : $ {{$treatment_planning_design_cost['tissue_supported_case']}}</p>
                    @endif
                    @if($treatment_planning_design->bone_supported_case == 1)
                        <p>Bone Supported Case : $ {{$treatment_planning_design_cost['bone_supported_case']}}</p>
                    @endif
                    @if($treatment_planning_design->provisional_planning == 1)
                        <p>Provisional Planning : $ {{$treatment_planning_design_cost['provisional_planning']}}</p>
                    @endif
                    
                    @if($treatment_planning_design->tooth_supported_case && ($treatment_planning_design->no_of_implants > 1))
                        <?php $temp_calc = $treatment_planning_design_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design->no_of_implants) - 1); ?>
                        <p>Additional Implant Cost : ${{$treatment_planning_design_cost['tooth_supported_case_additional']}} x {{ (abs($treatment_planning_design->no_of_implants) - 1) }} = ${{$temp_calc}}</p>
                    @endif
                    
                
                @endforeach
            @endif
            
            @if($key == 'treatment_planning_design_fabrication')
                <h4><strong><u>Treatment Planning (Design & Fabrication)</u></strong></h4>
                @foreach($serviceDetails[$key] as $treatment_planning_design_fabrication)
                <?php $treatment_planning_design_fabrication_cost = json_decode($treatment_planning_design_fabrication->cost,true); ?>
                
                    @if($treatment_planning_design_fabrication->bone_reduction_case == 1)
                        <p>Bone Reduction Case : $ {{$treatment_planning_design_fabrication_cost['bone_reduction_case']}}</p>
                    @endif
                    @if($treatment_planning_design_fabrication->tooth_supported_case == 1)
                        <p>Tooth Supported Case : $ {{$treatment_planning_design_fabrication_cost['tooth_supported_case']}}</p>
                    @endif
                    @if($treatment_planning_design_fabrication->tissue_supported_case == 1)
                        <p>Tissue Supported Case : $ {{$treatment_planning_design_fabrication_cost['tissue_supported_case']}}</p>
                    @endif
                    @if($treatment_planning_design_fabrication->bone_supported_case == 1)
                        <p>Bone Supported Case : $ {{$treatment_planning_design_fabrication_cost['bone_supported_case']}}</p>
                    @endif
                    @if($treatment_planning_design_fabrication->provisional_planning == 1)
                        <p>Provisional Planning : $ {{$treatment_planning_design_fabrication_cost['provisional_planning']}}</p>
                    @endif
                    
                    @if($treatment_planning_design_fabrication->tooth_supported_case && ($treatment_planning_design_fabrication->no_of_implants > 1))
                        <?php $temp_calc = $treatment_planning_design_fabrication_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design_fabrication->no_of_implants) - 1); ?>
                        <p>Additional Implant Cost : ${{$treatment_planning_design_fabrication_cost['tooth_supported_case_additional']}} x {{ (abs($treatment_planning_design_fabrication->no_of_implants) - 1) }} = ${{$temp_calc}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'digital_denture')
                <h4><strong><u>Digital Dentures</u></strong></h4>
                @foreach($serviceDetails[$key] as $digital_denture)
                <?php $digital_denture_cost = json_decode($digital_denture->cost,true); ?>
                
                    @if($digital_denture->print == 1)
                        <p>Print : $ {{$digital_denture_cost['print']}}</p>
                    @endif
                    @if($digital_denture->printing_planning == 1)
                        <p>Printing Planning : $ {{$digital_denture_cost['printing_planning']}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'orthodentic_treatment')
                <h4><strong><u>Orthodentic Treatment</u></strong></h4>
                @foreach($serviceDetails[$key] as $orthodentic_treatment)
                <?php $orthodentic_treatment_cost = json_decode($orthodentic_treatment->cost,true); ?>
                
                    @if($orthodentic_treatment->ortho_planning == 1)
                        <p>Ortho Planning : $ {{$orthodentic_treatment_cost['ortho_planning']}}</p>
                    @endif
                    @if($orthodentic_treatment->ortho_model_print == 1)
                        <?php $temp_calc = $orthodentic_treatment_cost['ortho_model_print'] * $orthodentic_treatment->no_of_prints; ?>
                        <p>Ortho Model Print : $ {{$orthodentic_treatment_cost['ortho_model_print']}} x {{ abs($orthodentic_treatment->no_of_prints) }} = ${{$temp_calc}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'radiology_report')
                <h4><strong><u>Radiology Report</u></strong></h4>
                @foreach($serviceDetails[$key] as $radiology_report)
                <?php $radiology_report_cost = json_decode($radiology_report->cost,true); ?>
                
                    @if($radiology_report->flat_rate == 1)
                        <p>Flat Rate : $ {{$radiology_report_cost['flat_rate']}}</p>
                    @endif
                    
                    @if($radiology_report->study_of_purpose)
                        <p>Study of Purpose: {{$radiology_report->study_of_purpose}}</p>
                    @endif
                    @if($radiology_report->job_description)
                        <p>Job Description: {{$radiology_report->job_description}}</p>
                    @endif
                    @if($radiology_report->date_of_birth)
                        <p>Date of Birth: {{$radiology_report->date_of_birth}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'metal_components')
                <h4><strong><u>metal_components</u></strong></h4>
                @foreach($serviceDetails[$key] as $metal_components)
                <?php $metal_components_cost = json_decode($metal_components->cost,true); ?>
                
                    @if($metal_components->guide_tubes == 1)
                        <p>Guide Tubes : $ {{$metal_components_cost['guide_tubes']}}</p>
                    @endif
                    @if($metal_components->fixation_pins == 1)
                        <p>Fixation Pins : $ {{$metal_components_cost['fixation_pins']}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'zygoma_guided_surgery')
                <h4><strong><u>Zygoma Guided Surgery (Design Only)</u></strong></h4>
                @foreach($serviceDetails[$key] as $zygoma_guided_surgery)
                <?php $zygoma_guided_surgery_cost = json_decode($zygoma_guided_surgery->cost,true); ?>
                
                    @if($zygoma_guided_surgery->quadrant_pterygoid == 1)
                        <p>Quadrant Pterygoid : $ {{$zygoma_guided_surgery_cost['quadrant_pterygoid']}}</p>
                    @endif
                    @if($zygoma_guided_surgery->full_pterygoid == 1)
                        <p>Full Pterygoid : $ {{$zygoma_guided_surgery_cost['full_pterygoid']}}</p>
                    @endif
                
                @endforeach
            @endif
            
            @endforeach
        
</div>
                            </td>
                            
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Total:</strong>
                            </td>
                            <td>
                               $ {{$order->total}}
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Shipping:</strong>
                            </td>
                            <td>
                               $ 0
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Amount Paid:</strong>
                            </td>
                            <td>
                               $ 0
                            </td>
                        </tr>
                        <tr class="total">
                            <td></td>
                            <td colspan="3">
                                <strong>Amount Due:</strong>
                            </td>
                            <td>
                               $ {{$order->total}}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>