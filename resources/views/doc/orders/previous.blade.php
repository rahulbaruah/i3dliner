@extends('doclayouts.master')

@section('page-title', 'Order History')

@section('style')
  @parent
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
  </style>
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/doc">Home</a></li>
    <li class="breadcrumb-item active">Order History</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <p>Total Orders</p>
                <h3>{{ $total_orders }}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <p>Amount Purchased</p>
                <h3>{{ $total_amount }}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
            
             <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table id="orders_table" class="table m-0">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Service</th>
                      <th>Order Date</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($orders as $order)
                      <tr>
                        <td>{{$order->id}}</td>
                        <td>{{showServicesFromArray($order->services)}}</td>
                        <td>{{$order->created_at->format('d/m/Y')}}</td>
                        <td>{{$order->total}}</td>
                        <td><span class="badge badge-default">{{$order->order_status->status ? $order->order_status->status : 'pending'}}</span></td>
                        <td>
                          <div class="btn-group" role="group">
                            <a href="/doc/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
              </div>-->
              <!-- /.card-footer -->
            </div>
            
        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script type="text/javascript">
  $(function () {
    $("#orders_table").DataTable({
    	"aaSorting": [],
        "columnDefs": [
            {
            "targets": 5,
            "orderable": false
            },
            {
            "targets": 3,
		        render: function ( data, type, row, meta ) {
		        	return '$' + data;
		        }
            }
            ]
    });
  });
</script>
@endsection