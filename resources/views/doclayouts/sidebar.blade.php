<!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/doc" class="nav-link {{ Request::is('doc') ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doc/new_order" class="nav-link">
              <i class="nav-icon fas fa-plus"></i>
              <p>
                New Order
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doc/orders" class="nav-link {{ Request::is('doc/orders') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Ongoing Orders
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doc/history" class="nav-link {{ Request::is('doc/history') ? 'active' : '' }}">
              <i class="nav-icon fas fa-project-diagram"></i>
              <p>
                Previous Orders
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doc/account" class="nav-link {{ Request::is('doc/account') ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Account Summary
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doc/price" class="nav-link {{ Request::is('doc/price') ? 'active' : '' }}">
              <i class="nav-icon fas fa-clipboard-list"></i>
              <p>
                Price Structure
              </p>
            </a>
          </li>
        </ul>
      </nav>
<!-- /.sidebar-menu -->