<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <!--<div class="float-right d-none d-sm-inline">
      Anything you want
    </div>-->
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date("Y") }} <a href="www.image3dconversion.com">Image3dconversion</a>.</strong> All rights reserved.
    
    <div class="float-right">
      +91-88763-95581 (India), +1-888-276-9811 (US+CANADA), +44-800-090-3841 (UK)
    </div>
  </footer>