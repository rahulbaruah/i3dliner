@extends('layouts.master')

@section('page-title', 'Order Details')

@section('style')
  @parent
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Order Details</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
    <div class="card">
        <div class="card-header bg-primary text-white">
                Doctor Information
        </div>
        <div class="card-body">
            <dl class="row">
              <dt class="col-sm-3">Doctor's Name</dt>
              <dd class="col-sm-9">{{$order->doctor_name}}</dd>
              
              <dt class="col-sm-3">Email</dt>
              <dd class="col-sm-9">{{$order->email}}</dd>
              
              <dt class="col-sm-3">Contact Number</dt>
              <dd class="col-sm-9">{{$order->contact_no}}</dd>
              
              <dt class="col-sm-3">Address</dt>
              <dd class="col-sm-9">{{$order->address}}</dd>
              
              <dt class="col-sm-3">Shipping Address</dt>
              <dd class="col-sm-9">{{$order->alt_address}}</dd>
            </dl>
        </div>
    </div>
    <div class="card">
            <div class="card-header bg-primary text-white">
                    Patient Information
            </div>
            <div class="card-body">
                <dl class="row">
                  <dt class="col-sm-3">Patient Name</dt>
                  <dd class="col-sm-9">{{$order->patient_name}}</dd>
                  
                  <dt class="col-sm-3">Surgery Date</dt>
                  <dd class="col-sm-9">{{$order->surgery_date->format('d/m/Y')}}</dd>
                </dl>
            </div>
    </div>
    <div class="card">
        <div class="card-header bg-primary text-white">
                Services with Cost
        </div>
        <div class="card-body">
            @foreach($services as $key=>$service)
            
            @if($key == 'bone_segmentation')
                <h4><strong><u>Bone Segmentation</u></strong></h4>
                @foreach($serviceDetails[$key] as $bone_segmentation)
                <?php $bone_segmentation_cost = json_decode($bone_segmentation->cost,true); ?>
                
                    @if($bone_segmentation->basic_conversion == 1)
                        Basic Conversion : $ {{$bone_segmentation_cost['basic_conversion']}}
                    @endif
                    @if($bone_segmentation->virtual_extraction == 1)
                        <br/>
                        Virtual Extraction : $ {{$bone_segmentation_cost['virtual_extraction']}}
                    @endif
                    @if($bone_segmentation->zygomatic_conversion == 1)
                        <br/>
                        Zygomatic Conversion : $ {{$bone_segmentation_cost['zygomatic_conversion']}}
                    @endif
                    @if($bone_segmentation->jaw_information)
                        <br/>
                        Jaw Information : {{$bone_segmentation->jaw_information}}
                    @endif
                    @if($bone_segmentation->order_description)
                        <br/>
                        Order Description : {{$bone_segmentation->order_description}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'treatment_planning_design')
                <h4><strong><u>Treatment Planning (Design Only)</u></strong></h4>
                @foreach($serviceDetails[$key] as $treatment_planning_design)
                <?php $treatment_planning_design_cost = json_decode($treatment_planning_design->cost,true); ?>
                
                    @if($treatment_planning_design->bone_reduction_case == 1)
                        Bone Reduction Case : $ {{$treatment_planning_design_cost['bone_reduction_case']}}
                    @endif
                    @if($treatment_planning_design->tooth_supported_case == 1)
                        <br/>
                        Tooth Supported Case : $ {{$treatment_planning_design_cost['tooth_supported_case']}}
                    @endif
                    @if($treatment_planning_design->tissue_supported_case == 1)
                        <br/>
                        Tissue Supported Case : $ {{$treatment_planning_design_cost['tissue_supported_case']}}
                    @endif
                    @if($treatment_planning_design->bone_supported_case == 1)
                        <br/>
                        Bone Supported Case : $ {{$treatment_planning_design_cost['bone_supported_case']}}
                    @endif
                    @if($treatment_planning_design->provisional_planning == 1)
                        <br/>
                        Provisional Planning : $ {{$treatment_planning_design_cost['provisional_planning']}}
                    @endif
                    
                    @if($treatment_planning_design->tooth_supported_case && ($treatment_planning_design->no_of_implants > 1))
                        <br/>
                        <?php $temp_calc = $treatment_planning_design_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design->no_of_implants) - 1); ?>
                        Additional Implant Cost : ${{$treatment_planning_design_cost['tooth_supported_case_additional']}} x {{ (abs($treatment_planning_design->no_of_implants) - 1) }} = ${{$temp_calc}}
                    @endif
                    
                    @if($treatment_planning_design->preferred_implant)
                        <br/>
                        Preferred Implant : {{$treatment_planning_design->preferred_implant}}
                    @endif
                    @if($treatment_planning_design->jaw_information)
                        <br/>
                        Jaw Information : {{$treatment_planning_design->jaw_information}}
                    @endif
                    @if($treatment_planning_design->restorative_plan)
                        <br/>
                        Restorative Plan : {{$treatment_planning_design->restorative_plan}}
                    @endif
                    @if($treatment_planning_design->surgery_date)
                        <br/>
                        Surgery Date : {{$treatment_planning_design->surgery_date}}
                    @endif
                    @if($treatment_planning_design->provisional_planning_reqd)
                        <br/>
                        Provisional Planning Reqd : {{$treatment_planning_design->provisional_planning_reqd}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'treatment_planning_design_fabrication')
                <h4><strong><u>Treatment Planning (Design & Fabrication)</u></strong></h4>
                @foreach($serviceDetails[$key] as $treatment_planning_design_fabrication)
                <?php $treatment_planning_design_fabrication_cost = json_decode($treatment_planning_design_fabrication->cost,true); ?>
                
                    @if($treatment_planning_design_fabrication->bone_reduction_case == 1)
                        Bone Reduction Case : $ {{$treatment_planning_design_fabrication_cost['bone_reduction_case']}}
                    @endif
                    @if($treatment_planning_design_fabrication->tooth_supported_case == 1)
                        <br/>
                        Tooth Supported Case : $ {{$treatment_planning_design_fabrication_cost['tooth_supported_case']}}
                    @endif
                    @if($treatment_planning_design_fabrication->tissue_supported_case == 1)
                        <br/>
                        Tissue Supported Case : $ {{$treatment_planning_design_fabrication_cost['tissue_supported_case']}}
                    @endif
                    @if($treatment_planning_design_fabrication->bone_supported_case == 1)
                        <br/>
                        Bone Supported Case : $ {{$treatment_planning_design_fabrication_cost['bone_supported_case']}}
                    @endif
                    @if($treatment_planning_design_fabrication->provisional_planning == 1)
                        <br/>
                        Provisional Planning : $ {{$treatment_planning_design_fabrication_cost['provisional_planning']}}
                    @endif
                    
                    @if($treatment_planning_design_fabrication->tooth_supported_case && ($treatment_planning_design_fabrication->no_of_implants > 1))
                        <br/>
                        <?php $temp_calc = $treatment_planning_design_fabrication_cost['tooth_supported_case_additional'] * (abs($treatment_planning_design_fabrication->no_of_implants) - 1); ?>
                        Additional Implant Cost : ${{$treatment_planning_design_fabrication_cost['tooth_supported_case_additional']}} x {{ (abs($treatment_planning_design_fabrication->no_of_implants) - 1) }} = ${{$temp_calc}}
                    @endif
                    
                    @if($treatment_planning_design_fabrication->preferred_implant)
                        <br/>
                        Preferred Implant : {{$treatment_planning_design_fabrication->preferred_implant}}
                    @endif
                    @if($treatment_planning_design_fabrication->jaw_information)
                        <br/>
                        Jaw Information : {{$treatment_planning_design_fabrication->jaw_information}}
                    @endif
                    @if($treatment_planning_design_fabrication->restorative_plan)
                        <br/>
                        Restorative Plan : {{$treatment_planning_design_fabrication->restorative_plan}}
                    @endif
                    @if($treatment_planning_design_fabrication->surgery_date)
                        <br/>
                        Surgery Date : {{$treatment_planning_design_fabrication->surgery_date}}
                    @endif
                    @if($treatment_planning_design_fabrication->provisional_planning_reqd)
                        <br/>
                        Provisional Planning Reqd : {{$treatment_planning_design_fabrication->provisional_planning_reqd}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'digital_denture')
                <h4><strong><u>Digital Dentures</u></strong></h4>
                @foreach($serviceDetails[$key] as $digital_denture)
                <?php $digital_denture_cost = json_decode($digital_denture->cost,true); ?>
                
                    @if($digital_denture->print == 1)
                        Print : $ {{$digital_denture_cost['print']}}
                    @endif
                    @if($digital_denture->printing_planning == 1)
                        <br/>
                        Printing Planning : $ {{$digital_denture_cost['printing_planning']}}
                    @endif
                    
                    @if($digital_denture->teeth_purchase_print	)
                        <br/>
                        Teeth Purchase Print: {{$digital_denture->teeth_purchase_print	}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'orthodentic_treatment')
                <h4><strong><u>Orthodentic Treatment</u></strong></h4>
                @foreach($serviceDetails[$key] as $orthodentic_treatment)
                <?php $orthodentic_treatment_cost = json_decode($orthodentic_treatment->cost,true); ?>
                
                    @if($orthodentic_treatment->ortho_planning == 1)
                        Ortho Planning : $ {{$orthodentic_treatment_cost['ortho_planning']}}
                    @endif
                    @if($orthodentic_treatment->ortho_model_print == 1)
                        <br/>
                        <?php $temp_calc = $orthodentic_treatment_cost['ortho_model_print'] * $orthodentic_treatment->no_of_prints; ?>
                        Ortho Model Print : $ {{$orthodentic_treatment_cost['ortho_model_print']}} x {{ abs($orthodentic_treatment->no_of_prints) }} = ${{$temp_calc}}
                    @endif
                    
                    @if($orthodentic_treatment->order_description	)
                        <br/>
                        Order Description: {{$orthodentic_treatment->order_description	}}
                    @endif
                    @if($orthodentic_treatment->jaw_information	)
                        <br/>
                        Jaw Information: {{$orthodentic_treatment->jaw_information	}}
                    @endif
                    @if($orthodentic_treatment->aligner_frequency	)
                        <br/>
                        Aligner Frequency: {{$orthodentic_treatment->aligner_frequency	}}
                    @endif
                    @if($orthodentic_treatment->anterior_movement	)
                        <br/>
                        Anterior Movement: {{$orthodentic_treatment->anterior_movement	}}
                    @endif
                    @if($orthodentic_treatment->chief_complaint	)
                        <br/>
                        Chief Complaint: {{$orthodentic_treatment->chief_complaint	}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'radiology_report')
                <h4><strong><u>Radiology Report</u></strong></h4>
                @foreach($serviceDetails[$key] as $radiology_report)
                <?php $radiology_report_cost = json_decode($radiology_report->cost,true); ?>
                
                    @if($radiology_report->flat_rate == 1)
                        Flat Rate : $ {{$radiology_report_cost['flat_rate']}}
                    @endif
                    
                    @if($radiology_report->study_of_purpose	)
                        <br/>
                        Study of Purpose: {{$radiology_report->study_of_purpose	}}
                    @endif
                    @if($radiology_report->job_description	)
                        <br/>
                        Job Description: {{$radiology_report->job_description	}}
                    @endif
                    @if($radiology_report->date_of_birth	)
                        <br/>
                        Date of Birth: {{$radiology_report->date_of_birth	}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'metal_components')
                <h4><strong><u>metal_components</u></strong></h4>
                @foreach($serviceDetails[$key] as $metal_components)
                <?php $metal_components_cost = json_decode($metal_components->cost,true); ?>
                
                    @if($metal_components->guide_tubes == 1)
                        Guide Tubes : $ {{$metal_components_cost['guide_tubes']}}
                    @endif
                    @if($metal_components->fixation_pins == 1)
                        <br/>
                        Fixation Pins : $ {{$metal_components_cost['fixation_pins']}}
                    @endif
                
                @endforeach
            @endif
            
            @if($key == 'zygoma_guided_surgery')
                <h4><strong><u>Zygoma Guided Surgery (Design Only)</u></strong></h4>
                @foreach($serviceDetails[$key] as $zygoma_guided_surgery)
                <?php $zygoma_guided_surgery_cost = json_decode($zygoma_guided_surgery->cost,true); ?>
                
                    @if($zygoma_guided_surgery->quadrant_pterygoid == 1)
                        Quadrant Pterygoid : $ {{$zygoma_guided_surgery_cost['quadrant_pterygoid']}}
                    @endif
                    @if($zygoma_guided_surgery->full_pterygoid == 1)
                        <br/>
                        Full Pterygoid : $ {{$zygoma_guided_surgery_cost['full_pterygoid']}}
                    @endif
                
                @endforeach
            @endif
            
            @endforeach
            
        <hr/>
        <h4>Total: $ {{$order->total}}</h4>
@if($order->order_status->status != 'completed')
<a data-id="{{$order->id}}" href="#" class="btn btn-success orderCompleteBtn" >Complete Order</a>
@else
<p class="text-success">Order Complete</p>
@endif
</div>
</div>
            
        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script>
  $(function () {
      $(document).on('click','.orderCompleteBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Complete this order',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/order/'+$order_id+'/status',
                		        data : {
                  				    status : 'completed'
                  				  },
                		        /*dataType: 'json',*/
                		        success:function(data){
                		            $.alert("Order Completed");
                		            window.location.reload();
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              /*$.alert('Canceled!');*/
              return;
            }
        }
      });
      return;
    });
  });
</script>
@endsection