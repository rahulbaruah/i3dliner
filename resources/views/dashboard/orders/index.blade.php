@extends('layouts.master')

@section('page-title', 'Ongoing Orders')

@section('style')
  @parent
<!-- DataTables -->
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Ongoing Orders</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">

<div class="card">
            <div class="card-header">
              <h3 class="card-title">ONGOING ORDERS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            	<div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Service</th>
                  <th>Doctor</th>
                  <th>Order Date</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($processing_order_details as $order)
                <tr>
                  <td>{{$order->id}}</td>
                  <td>{{showServicesFromArray($order->services)}}</td>
                  <td>{{$order->doctor_name}}</td>
                  <td>{{$order->created_at->format('d/m/Y')}}</td>
                  <td>{{$order->total}}</td>
                  <td>Phase-{{(int)$order->order_status->phase}}</td>
                  <td>
                    <!--<div class="btn-group" role="group">
                          <a href="/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                          <button data-id="{{$order->id}}" type="button" class="btn btn-success btn-sm phaseUpdateBtn">Process to Next Phase</button>
                          @if($order->order_status->case_link)
                          <a href="/order/{{$order->id}}/upload" class="btn btn-warning btn-sm" title="Update link for client approval">Update Link</a>
                          @else
                          <a href="/order/{{$order->id}}/upload" class="btn btn-primary btn-sm" title="Upload link for client approval">Upload Link</a>
                          @endif
                    </div>-->
                    <div class="dropdown">
                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Select
                      </button>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="/order/{{$order->id}}">Details</a>
                        <a data-id="{{$order->id}}" class="dropdown-item orderCancelBtn" href="#">Cancel</a>
                        <a class="dropdown-item" href="/order/{{$order->id}}/upload">View/Upload File</a>
                        <a data-id="{{$order->id}}" class="dropdown-item invalidFileBtn" href="#">Invalid File</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>

<!-- Info boxes -->
<h4>All Orders</h4>
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box bg-info">

              <div class="info-box-content">
                <span class="info-box-text">TOTAL ORDERS</span>
                <span class="info-box-number">{{$total_orders}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3 bg-success">

              <div class="info-box-content">
                <span class="info-box-text">DELIVERED</span>
                <span class="info-box-number">{{$completed_orders}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3 bg-warning">
              <div class="info-box-content">
                <span class="info-box-text">ONGOING ORDERS</span>
                <span class="info-box-number">{{$processing_orders}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3 bg-danger">
              <div class="info-box-content">
                <span class="info-box-text">CANCELLED</span>
                <span class="info-box-number">{{$cancelled_orders}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

<div class="card">
            <div class="card-header">
              <h3 class="card-title">CANCELLED ORDERS</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Service</th>
                  <th>Doctor</th>
                  <th>Order Date</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cancelled_order_details as $order)
                <tr>
                  <td>{{$order->id}}</td>
                  <td>{{showServicesFromArray($order->services)}}</td>
                  <td>{{$order->doctor_name}}</td>
                  <td>{{$order->created_at->format('d/m/Y')}}</td>
                  <td>{{$order->total}}</td>
                  <td><span class="badge badge-danger">cancelled</span></td>
                  <td>
                    <a href="/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>

        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script>
  $(function () {
    $("#example1").DataTable({
    	"aaSorting": [],
        "columnDefs": [
            {
            "targets": 6,
            "orderable": false
            },
            /*{
            "targets": 2,
		        render: function ( data, type, row, meta ) {
		        	return 'Dr. ' + data;
		        }
            },*/
            {
            "targets": 4,
		        render: function ( data, type, row, meta ) {
		        	return '$' + data;
		        }
            }
            ]
    });
    
    $("#example2").DataTable({
        "columnDefs": [
        	{
	            "targets": 6,
	            "orderable": false
            },
            {
            "targets": 2,
		        render: function ( data, type, row, meta ) {
		        	return 'Dr. ' + data;
		        }
            },
            {
            "targets": 4,
		        render: function ( data, type, row, meta ) {
		        	return '$ ' + data;
		        }
            }
        ]
    });
    
    /*$(document).on('click','.phaseUpdateBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Process to next phase',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/order/'+$order_id+'/phase',
                		        success:function(data){
                		            $.alert('Successfully Updated!');
                		            window.location.reload();
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              // $.alert('Canceled!');
              return;
            }
        }
      });
      return;
    });*/
    
    $(document).on('click','.orderCancelBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Cancel this order',
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/order/'+$order_id+'/status',
                		        data : {
                  				    status : 'cancelled'
                  				  },
                		        /*dataType: 'json',*/
                		        success:function(data){
                		            window.location = "{{url('/orders')}}";
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              /*$.alert('Canceled!');*/
              return;
            }
        }
      });
      return;
    });
    
    $(document).on('click','.invalidFileBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Send Invalid File Notification',
        buttons: {
            confirm: {
                btnClass: 'btn-warning',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/order/'+$order_id+'/invalid',
                		        /*dataType: 'json',*/
                		        success:function(data){
                		            $.alert('Sent notification to the doctor');
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              /*$.alert('Canceled!');*/
              return;
            }
        }
      });
      return;
    });
  });
</script>
@endsection