@extends('layouts.master')

@section('page-title', 'Upload Link')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/orders">Orders</a></li>
    <li class="breadcrumb-item active">Upload Link</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            
    <div class="card">
        <div class="card-header bg-primary text-white">
                Order Id: {{$order->id}}
        </div>
        <div class="card-body">
<form method="post" action="/order/{{$order->id}}/upload">
    @csrf
  <div class="form-group">
    <label for="dropbox_url">DropBox Url</label>
    <input name="case_link" type="text" class="form-control" id="dropbox_url" aria-describedby="dropbox_urlHelp" value="{{$order->order_status->case_link}}">
    <small id="dropbox_urlHelp" class="form-text text-muted">For client approval</small>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
        </div>
    </div>
            
        </div>
    </section>
@endsection