@extends('layouts.master')

@section('page-title', 'All Orders')

@section('style')
  @parent
<!-- DataTables -->
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
  <link rel="stylesheet" href="/css/daterangepicker.css">
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">All Orders</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
<div class="card">
            <div class="card-header">
              <h3 class="card-title">ALL ORDERS</h3>
              <a href="/allorders?status=cancelled" class="btn btn-default float-right btn-sm">Cancelled Orders</a>
              <a href="/allorders" class="btn btn-primary float-right btn-sm">All Orders</a>
              
              <div class="float-right">
<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto">
    <i class="fa fa-calendar-alt"></i>&nbsp;
    <span>Date Filter</span> <i class="fa fa-caret-down"></i>
</div>
</div>
              
            </div>
            <!-- /.card-header -->
            <div class="card-body">

              
              <div class="table-responsive">
              <table id="allOrdersTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Service</th>
                  <th>Doctor</th>
                  <th>Order Date</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <?php $temp_order_status = $order->order_status->status; 
                    if($temp_order_status == 'processing') {
                        $badge = 'badge-info';
                    }
                    elseif($temp_order_status == 'completed') {
                        $badge = 'badge-success';
                    }
                    elseif($temp_order_status == 'cancelled') {
                        $badge = 'badge-danger';
                    }
                    else {
                        $badge = 'badge-warning';
                    }
                ?>
                <tr>
                  <td>{{$order->id}}</td>
                  <td>{{showServicesFromArray($order->services)}}</td>
                  <td>{{$order->doctor_name}}</td>
                  <td>{{$order->created_at->format('d/m/Y')}}</td>
                  <td>{{$order->total}}</td>
                  <td><span class="badge {{$badge}}">{{$order->order_status->status}}</span></td>
                  <td>
                    <a href="/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>

        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script>
  $(function () {
    $("#allOrdersTable").DataTable({
        "columnDefs": [
        	{
	            "targets": 6,
	            "orderable": false
            },
            {
            "targets": 2,
		        render: function ( data, type, row, meta ) {
		        	return 'Dr. ' + data;
		        }
            },
            {
            "targets": 4,
		        render: function ( data, type, row, meta ) {
		        	return '$ ' + data;
		        }
            }
        ]
    });
    
  });
</script>
<script type="text/javascript">
$(function() {

    var start = moment().subtract(29, 'days');
    var end = moment();
    
    @if($from && $to)
    start = moment("{{$from}}",'YYYY-MM-DD');
    end = moment("{{$to}}",'YYYY-MM-DD');
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    @endif
    
    function cb(start, end) {
        /*$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));*/
        
        var from = start.format('YYYY-MM-DD');
        var to = end.format('YYYY-MM-DD');
        var pageurl = '/allorders';
        var urlparams = {
          'status' : '{{$status}}',
          'from' : from,
          'to' : to
        };
        urlparams = jQuery.param(urlparams);
        var url = pageurl + '?' + urlparams;
        window.location.href = url;
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    /*cb(start, end);*/

});
</script>
@endsection