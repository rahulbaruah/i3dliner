@extends('layouts.master')

@section('page-title', 'Doctor Details')

@section('style')
  @parent
<!-- DataTables -->
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/doctors">Doctors</a></li>
    <li class="breadcrumb-item active">Doctor Details</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-4">
            <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="row">
              <div class="col-md-4">
                <img class="img img-fluid rounded-circle" src="/vendor/adminlte/img/avatar.png"></img>
              </div>
              <div class="col-md-8">
                <strong>Name:</strong>{{$doctor->name}}<br/>
                <strong>Email:</strong>{{$doctor->name}}<br/>
                <strong>Contact:</strong>{{$doctor->mobile}}<br/>
                <strong>Address:</strong>{{$doctor->address}}<br/>
              </div>
              </div>
            </div>
            </div>
            </div>
            <div class="col-lg-4">
              <div class="small-box bg-info">
                <div class="inner">
                  <p>Total Orders</p>
                  <h3>{{$doctor->total_orders}}</h3>
                </div>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="small-box bg-success">
                <div class="inner">
                  <p>Total Purchase Amount</p>
                  <h3>$ {{$doctor->total_amount}}</h3>
                </div>
              </div>
            </div>
          </div>
          
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">ORDER HISTORY</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
              <table id="ordersTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Service</th>
                  <th>Date</th>
                  <th>Price</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($doctor->orders as $order)
                <tr>
                  <td>{{$order->id}}</td>
                  <td>{{showServicesFromArray($order->services)}}</td>
                  <td>{{$order->created_at->format('d/m/Y')}}</td>
                  <td>{{$order->total}}</td>
                  <td>
                    <a href="/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
              </div>
            </div>
            <!-- /.card-body -->
          </div>  
          
        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
$("#ordersTable").DataTable({
    	"aaSorting": [],
        "columnDefs": [
            {
            "targets": 4,
            "orderable": false
            },
            {
            "targets": 3,
		        render: function ( data, type, row, meta ) {
		        	return '$' + data;
		        }
            }
            ]
    });
</script>
@endsection