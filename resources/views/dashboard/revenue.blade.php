@extends('layouts.master')

@section('page-title', 'Revenue')

@section('style')
  @parent
  <link rel="stylesheet" href="/css/daterangepicker.css">
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active">Revenue Page</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- LINE CHART -->
            <div class="card card-info">
              <div class="card-header">
                <!--<h3 class="card-title">Line Chart</h3>-->

                <div class="card-tools">
                  <!--<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>-->
                  <div class="float-right">
                    <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto">
                        <i class="fa fa-calendar-alt"></i>&nbsp;
                        <span>Date Filter</span> <i class="fa fa-caret-down"></i>
                    </div>
                    </div>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="lineChart" style="height:250px; min-height:250px"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
        <!-- Bar Chart -->
        <div class="card card-success">
              <div class="card-header">
                <!--<h3 class="card-title">Bar Chart</h3>-->

                <div class="card-tools">
                  <!--<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>-->
                  <!--<div class="float-right">
                    <div id="reportrangeCountry" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto">
                        <i class="fa fa-calendar-alt"></i>&nbsp;
                        <span>Date Filter</span> <i class="fa fa-caret-down"></i>
                    </div>
                    </div>-->
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barChart" style="height:230px; min-height:230px"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
  </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/chart.js/Chart.min.js"></script>
<script src="/js/moment.min.js"></script>
<script src="/js/daterangepicker.js"></script>
<script type="text/javascript">
  $(function () {
    var areaChartData = {
      labels  : @json($labels),
      datasets: [
        {
          label               : 'Revenue',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          /*pointRadius          : false,*/
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : @json($values)
        }
      ]
    };

    var areaChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : false,
          },
          ticks: {
                    /*Include a dollar sign in the ticks*/
                    callback: function(value, index, values) {
                        return '$' + value;
                    }
                }
        }
        ]
      }
    };
    
    var lineChartCanvas = $('#lineChart').get(0).getContext('2d');
    
    var lineChartOptions = jQuery.extend(true, {}, areaChartOptions);
    lineChartOptions.datasetFill = false
    
    var lineChartData = jQuery.extend(true, {}, areaChartData);
    
    lineChartData.datasets[0].fill = false;
    /*lineChartData.datasets[1].fill = false;*/
    

    var lineChart = new Chart(lineChartCanvas, { 
      type: 'line',
      data: lineChartData, 
      options: lineChartOptions
    });
  

    var start = moment().subtract(6, 'months');
    var end = moment();
    
    @if($from && $to)
    start = moment("{{$from}}",'YYYY-MM-DD');
    end = moment("{{$to}}",'YYYY-MM-DD');
    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    @endif
    
    function cb(start, end) {
        /*$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));*/
        
        var from = start.format('YYYY-MM-DD');
        var to = end.format('YYYY-MM-DD');
        var pageurl = '/revenue';
        var urlparams = {
          'from' : from,
          'to' : to
        };
        urlparams = jQuery.param(urlparams);
        var url = pageurl + '?' + urlparams;
        window.location.href = url;
    }

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'Last 3 Months': [moment().subtract(3, 'months'), moment()],
           'Last 6 Months': [moment().subtract(6, 'months'), moment()],
           'Last 12 Months': [moment().subtract(12, 'months'), moment()]
        }
    }, cb);

    /*cb(start, end);*/
});

$(function () {
    var areaChartData = {
      labels  : @json($countries_labels),
      datasets: [
        {
          label               : 'Revenue',
          backgroundColor     : [  
                                    '#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'
                                 ],
         /* borderColor         : [  
                                    "rgb(255, 99, 132)",
                                    "rgb(255, 159, 64)",
                                    "rgb(255, 205, 86)",
                                    "rgb(75, 192, 192)",
                                    "rgb(54, 162, 235)",
                                    "rgb(153, 102, 255)",
                                    "rgb(201, 203, 207)"
                                 ],*/
            borderWidth :   1,
          data                : @json($country_values)
        }
      ]
    }
    
    var barChartCanvas = $('#barChart').get(0).getContext('2d');
    var barChartData = jQuery.extend(true, {}, areaChartData);
    var temp0 = areaChartData.datasets[0];
    barChartData.datasets[0] = temp0;

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : false,
          },
          ticks: {
                    /*Include a dollar sign in the ticks*/
                    callback: function(value, index, values) {
                        return '$' + value;
                    }
                }
        }
        ]
      }
    };

    var barChart = new Chart(barChartCanvas, {
      type: 'bar', 
      data: barChartData,
      options: barChartOptions
    });
    
});

/*function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}*/
</script>
@endsection