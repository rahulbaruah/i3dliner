@extends('layouts.master')

@section('page-title', 'Dashboard')

@section('style')
  @parent
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
  <style type="text/css">
    .products-list .product-info {
      margin-left: 10px;
  }
  </style>
@endsection

@section('breadcrumb')
<!--<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active">Starter Page</li>
</ol>-->
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <p>Total Orders</p>
                <h3>{{$total_orders}}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <p>Delivered</p>
                <h3>{{$completed_orders}}</h3>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <!--<div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <p>Ongoing Orders</p>
                <h3>{{$processing_orders}}</h3>
              </div>
            </div>
          </div>-->
          <!-- ./col -->
        </div>
        <!-- /.row -->
        
        <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">New Orders</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table id="example2" class="table m-0">
                    <thead>
                    <tr>
                      <th>Order ID</th>
                      <th>Service</th>
                      <th>Doctor</th>
                      <th>Order Date</th>
                      <th>Price</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($orders as $order)
                      <tr>
                        <td>{{$order->id}}</td>
                        <td>{{showServicesFromArray($order->services)}}</td>
                        <td>{{$order->doctor_name}}</td>
                        <td>{{$order->created_at->format('d/m/Y')}}</td>
                        <td>{{$order->total}}</td>
                        <td><span class="badge badge-warning">Pending</span></td>
                        <td>
                          <div class="btn-group" role="group">
                            <a href="/order/{{$order->id}}" class="btn btn-info btn-sm">Details</a>
                            <button data-id="{{$order->id}}" type="button" class="btn btn-success btn-sm orderProcessBtn">Process</button>
                            <button data-id="{{$order->id}}" type="button" class="btn btn-danger btn-sm orderCancelBtn">Cancel</button>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
              </div>-->
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
        
        <div class="row">
          <div class="col-md-8">
            <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Best Selling Services</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table m-0">
                    <tbody>
                    @foreach($services as $key=>$service)
                    <tr>
                      <td>{{styleString($key)}}</td>
                      <td><span class="badge badge-primary">{{$service_count[$key]}} Sales</span></td>
                      <td><span class="badge badge-success">$ {{$service}} Collected</span></td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
              </div>-->
              <!-- /.card-footer -->
            </div>
          </div>
        <!-- PRODUCT LIST -->
        <div class="col-md-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Top 5 Doctors</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  @foreach($topDocs as $key=>$docs)
                  <li class="item">
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title text-capitalize">{{$key}}
                        <span class="badge badge-success float-right">${{$docs}}</span></a>
                    </div>
                  </li>
                  @endforeach
                  <!-- /.item -->
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
        </div>
        
        <div class="col-md-4">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Revenue By Country</h3>

                <!--<div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="chart-responsive">
                      <canvas id="pieChart" height="150"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              
            </div>
          </div>
        </div>
        <!-- Main row -->
    </div>
  </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/chart.js/Chart.min.js"></script>
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript">
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = {
      labels: @json(array_keys($topCountries)),
      datasets: [
        {
          data: @json(array_values($topCountries)),
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
    var pieOptions     = {
      legend: {
        display: true,
        position: 'right'
      }
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'doughnut',
      data: pieData,
      options: pieOptions      
    });
    
    /*$('#example2').DataTable({
      "paging": true,
      "pageLength": 5,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "columnDefs": [
            {
            "targets": 4,
		        render: function ( data, type, row, meta ) {
		        	return '$ ' + data;
		        }
            }
        ]
    });*/
    
    $(document).on('click','.orderProcessBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Process this order',
        buttons: {
            confirm: {
                btnClass: 'btn-primary',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/order/'+$order_id+'/status',
                		        data : {
                  				    status : 'processing'
                  				  },
                		        /*dataType: 'json',*/
                		        success:function(data){
                		            window.location = "{{url('/orders')}}";
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              /*$.alert('Canceled!');*/
              return;
            }
        }
      });
      return;
    });
    
    $(document).on('click','.orderCancelBtn', function(e){
      e.preventDefault();
      var $order_id = $(this).data('id');
      $.confirm({
        title: 'Confirm!',
        content: 'Cancel this order',
        buttons: {
            confirm: {
                btnClass: 'btn-danger',
                action: function() {
                $.ajax({
                		        type: 'GET',
                		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                		        url: '/order/'+$order_id+'/status',
                		        data : {
                  				    status : 'cancelled'
                  				  },
                		        /*dataType: 'json',*/
                		        success:function(data){
                		            window.location = "{{url('/orders')}}";
                		        },
                		        error: function(data){
                		            alert("An error has occurred. Please try again.");
                		        }
                		    });
              return;
                }
            },
            cancel: function () {
              /*$.alert('Canceled!');*/
              return;
            }
        }
      });
      return;
    });
    
</script>
@endsection