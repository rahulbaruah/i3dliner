@extends('layouts.master')

@section('page-title', 'Users')

@section('style')
  @parent
  <link rel="stylesheet" href="/vendor/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="/vendor/jquery-confirm/jquery-confirm.min.css">
  </style>
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item active">Users</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
                         <!-- TABLE: LATEST ORDERS -->
            <a href="/users/create" class="btn btn-primary" title="Add New User"><i class="fas fa-plus"></i> New User</a>
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  
                  <table id="usersTable" class="table table-bordered table-hover" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th data-col="show">Sl</th>
			<th data-col="show">Name</th>
			<th data-col="show">E-mail</th>
			<th data-col="show">Role</th>
			<th data-col="show">Action</th>
		</tr>
	</thead>
	<tbody>
@foreach ($users as $key=>$user)
		<tr>
			<td>{{ ($key+1) }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->role }}</td>
			<td>
				<a title="Edit User" class="btn btn-primary btn-sm" href="/users/{{$user->id}}/edit" role="button" title="Edit">
					<i class="fas fa-edit"></i>
				</a>
				@if ($user->id > 1)
				<a title="Delete User" class="delBtn btn btn-danger btn-sm" href="#" data-href="/photos/{{$user->id}}" role="button" title="Delete">
					<i class="fas fa-trash-alt"></i>
				</a>
				@endif
			</td>
		</tr>
@endforeach
	</tbody>
</table>
                  
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <!--<div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>
                <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>
              </div>-->
              <!-- /.card-footer -->
            </div>
            
        </div>
    </section>
@endsection

@section('script')
  @parent
<script src="/vendor/adminlte/plugins/datatables/jquery.dataTables.js"></script>
<script src="/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script src="/vendor/jquery-confirm/jquery-confirm.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		
		var table = $('#usersTable').DataTable({
    		"aaSorting": [[ 0, "desc" ]],
    	});
    	
    	$(document).on('click','.delBtn', function(e){
          e.preventDefault();
          var url = $(this).data("href");
          $.confirm({
            title: 'Confirm!',
            content: 'Delete this user',
            buttons: {
                confirm: {
                    btnClass: 'btn-danger',
                    action: function() {
                    $.ajax({
                    		        type: 'DELETE',
                    		        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    		        url: url,
                    		        success:function(data){
                    		            window.location.reload();
                    		        },
                    		        error: function(data){
                    		            alert("An error has occurred. Please try again.");
                    		        }
                    		    });
                  return;
                    }
                },
                cancel: function () {
                  /*$.alert('Canceled!');*/
                  return;
                }
            }
          });
          return;
        });
	});
</script>
@endsection