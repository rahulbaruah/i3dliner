@extends('layouts.master')

@section('page-title', 'Edit User')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item"><a href="/users">Users</a></li>
    <li class="breadcrumb-item active">Edit User</li>
</ol>
@endsection

@section('content')
<!-- Main content -->
    <section class="content">
        <div class="container-fluid">
                         <!-- TABLE: LATEST ORDERS -->
            <div class="card">
              <!-- /.card-header -->
              <form class="form-horizontal" role="form" action="/users/{{$user->id}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
              <div class="card-body">
						<div class="form-group">
							<label class="col-sm-3 control-label">Name:</label>
							<div class="col-sm-9">
							<input type="text" class="form-control" name="name" value="{{$user->name}}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Email:</label>
							<div class="col-sm-9">
							<input type="email" class="form-control" name="email" value="{{$user->email}}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Role:</label>
							<div class="col-sm-9">
							<select name="role" class="form-control">
							    <option value="">None</option>
							    <option value="admin">Admin</option>
							    <option value="editor">Editor</option>
							    <option value="viewer">Viewer</option>
							    <!--<option value="publisher">Publisher</option>-->
							</select>
							</div>
						</div>
						@section('script')
						    @parent
						<script type="text/javascript">
						    $(document).ready(function(){
						       $('select[name="role"]').val("{{$user->role}}"); 
						    });
						</script>
						@endsection
						<div class="form-group">
							<label class="col-sm-3 control-label">Password:</label>
							<div class="col-sm-9">
							<input type="password" class="form-control" name="password" value="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Confirm Password:</label>
							<div class="col-sm-9">
							<input type="text" class="form-control" name="password_confirmation" value="">
							</div>
						</div>
			
	
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
			    <button name="action" type="submit" value="SUBMIT" class="btn btn-primary">Submit</button>
              </div>
            </form>
              <!-- /.card-footer -->
            </div>
            
        </div>
    </section>
@endsection