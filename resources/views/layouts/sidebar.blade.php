<!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link {{ Request::is('/') ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/orders" class="nav-link {{ Request::is('orders') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Ongoing Orders
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/allorders" class="nav-link {{ Request::is('allorders') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                All Orders
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/doctors" class="nav-link {{ Request::is('doctors') ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-md"></i>
              <p>
                Doctors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/users" class="nav-link {{ Request::is('users') ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User Management
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/revenue" class="nav-link {{ Request::is('revenue') ? 'active' : '' }}">
              <i class="nav-icon fas fa-chart-area"></i>
              <p>
                Revenue
              </p>
            </a>
          </li>
        </ul>
      </nav>
<!-- /.sidebar-menu -->