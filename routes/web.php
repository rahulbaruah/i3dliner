<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('dashboard.index');
})->middleware('auth');*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/orders', 'OrdersController@index');

Route::get('/order/{id}', 'OrdersController@show');

Route::get('/order/{id}/upload', 'OrdersController@ShowUploadLink');
Route::post('/order/{id}/upload', 'OrdersController@UploadLink');

Route::get('/order/{id}/status', 'OrdersController@StatusUpdate');

Route::get('/order/{id}/phase', 'OrdersController@PhaseUpdate');

Route::get('/order/{id}/invalid', 'OrdersController@InvalidFileUpdate');

Route::get('/allorders', 'OrdersController@ShowAllOrders');

Route::get('/doctors', 'DoctorsController@index');

Route::get('/doctor/{id}', 'DoctorsController@show');

Route::get('/fixorderstatus', 'OrdersController@FixOrderStatus');

Route::post('/push','PushController@store');
Route::get('/push','PushController@push')->name('push');

Route::get('/doctorfix','DoctorsController@fix');

Route::get('/revenue', 'RevenueController@index');

Route::resource('users', 'UsersController');

Route::namespace('Doc')->group(function () {
    Route::prefix('doc')->group(function () {
        Route::get('/', 'HomeController@index');
        
        Route::get('login', 'LoginController@showLoginFormUsers');
        Route::post('login', 'LoginController@checklogin');
        Route::get('logout', 'LoginController@logout');
        Route::post('logout', 'LoginController@logout');
        
        Route::get('/order/{id}', 'OrdersController@show');
        Route::get('/orders', 'OrdersController@ShowOngoing');
        Route::get('/history', 'OrdersController@ShowHistory');
        Route::get('/order/{id}/approve', 'OrdersController@Approve');
        Route::get('/invoice/{id}', 'InvoiceController@ShowInvoice');
        
        Route::get('/new_order',function(){
            return view('doc.orders.new');
        });
        
        Route::resource('profile', 'ProfileController');
        
        Route::get('/account', 'AccountController@index');
        
        Route::get('/price', function(){
            return view('doc.price_structure');
        });
    });
});